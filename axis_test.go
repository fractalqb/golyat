package golyat

import "testing"

func TestSpreader(t *testing.T) {
	t.Run("1 shape", func(t *testing.T) {
		r := Rect[float64]{Min: Pt(0.0, 0.0), Max: Pt(10.0, 10.0)}
		Spread(X, 0.0, 1.0).Shapes(-10, 10, &r)
		if y := r.Min.Y(); y != 0 {
			t.Errorf("min-y: %f", y)
		}
		if y := r.Max.Y(); y != 10 {
			t.Errorf("max-y: %f", y)
		}
		if x := r.Min.X(); x != -5 {
			t.Errorf("min-x: %f", x)
		}
		if x := r.Max.X(); x != 5 {
			t.Errorf("max-x: %f", x)
		}
	})
	t.Run("two centers", func(t *testing.T) {
		r0 := Rect[float64]{Min: Pt(0.0, 0.0), Max: Pt(10.0, 10.0)}
		r1 := r0
		Spread(X, 0.5, 0.5).ShapesBetween(-15, 15, &r0, &r1)
		bb := BBox(&r0, &r1)
		if y := bb.Min.Y(); y != 0 {
			t.Errorf("min-y: %f", y)
		}
		if y := bb.Max.Y(); y != 10 {
			t.Errorf("max-y: %f", y)
		}
		if x := bb.Min.X(); x != -15 {
			t.Errorf("min-x: %f", x)
		}
		if x := bb.Max.X(); x != 15 {
			t.Errorf("max-x: %f", x)
		}
	})
}

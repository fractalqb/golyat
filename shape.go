package golyat

import "iter"

type Shape[S Scalar] interface {
	Hull(yield func(Point[S]) bool)
	Move(dx, dy S)
}

type BBoxer[S Scalar] interface {
	BBox() Rect[S]
}

func BBox[S Scalar](s ...Shape[S]) (bb Rect[S]) {
	if len(s) == 0 {
		return
	}
	if bx, ok := s[0].(BBoxer[S]); ok {
		bb = bx.BBox()
	} else {
		bb = BBoxOf(s[0].Hull)
	}
	for _, e := range s[1:] {
		var b Rect[S]
		if bx, ok := e.(BBoxer[S]); ok {
			b = bx.BBox()
		} else {
			b = BBoxOf(e.Hull)
		}
		bb.Min[X] = min(bb.Min[X], b.Min[X])
		bb.Min[Y] = min(bb.Min[Y], b.Min[Y])
		bb.Max[X] = max(bb.Max[X], b.Max[X])
		bb.Max[Y] = max(bb.Max[Y], b.Max[Y])
	}
	return
}

func BBoxOf[S Scalar](pit iter.Seq[Point[S]]) (bb Rect[S]) {
	next, _ := iter.Pull(pit)
	p, ok := next()
	if !ok {
		return
	}
	bb.Min, bb.Max = p, p
	for p, ok = next(); ok; p, ok = next() {
		bb.Min[X] = min(bb.Min[X], p[X])
		bb.Min[Y] = min(bb.Min[Y], p[Y])
		bb.Max[X] = max(bb.Max[X], p[X])
		bb.Max[Y] = max(bb.Max[Y], p[Y])
	}
	return
}

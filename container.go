package golyat

type Container[S Scalar] []Shape[S]

var (
	_ Shape[float64]  = (Container[float64])(nil)
	_ BBoxer[float64] = (Container[float64])(nil)
)

func (c Container[S]) Hull(yield func(Point[S]) bool) {
	// TODO Container[S].Hull()
	c.BBox().Hull(yield)
}

func (c Container[S]) Move(dx, dy S) {
	for _, e := range c {
		e.Move(dx, dy)
	}
}

func (c Container[S]) BBox() Rect[S] { return BBox(c...) }

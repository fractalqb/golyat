package golyat

type Line[S Scalar] struct{ P, D Point[S] }

func (l *Line[S]) Q() Point[S] { return Pt(l.P[X]+l.D[X], l.P[Y]+l.D[Y]) }

func (l *Line[S]) Hull(yield func(Point[S]) bool) {
	if !yield(l.P) {
		return
	}
	yield(l.Q())
}

func (l *Line[S]) Move(dx, dy S) { l.P.Move(dx, dy) }

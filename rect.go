package golyat

type Rect[S Scalar] struct{ Min, Max Point[S] }

var (
	_ Shape[float64]  = (*Rect[float64])(nil)
	_ BBoxer[float64] = (*Rect[float64])(nil)
)

func NewRect[S Scalar](min, max Point[S]) *Rect[S] {
	if min[X] > max[X] {
		min[X], max[X] = max[X], min[X]
	}
	if min[Y] > max[Y] {
		min[Y], max[Y] = max[Y], min[Y]
	}
	return &Rect[S]{Min: min, Max: max}
}

func (r Rect[S]) Extent(a Axis) S { return r.Max[a] - r.Min[a] }

func (r Rect[S]) Frac(a Axis, f S) S {
	return r.Min[a] + f*r.Extent(a)
}

func (r Rect[S]) Hull(yield func(Point[S]) bool) {
	if !yield(r.Max) {
		return
	}
	if !yield(Pt(r.Min[X], r.Max[Y])) {
		return
	}
	if !yield(r.Min) {
		return
	}
	yield(Pt(r.Max[X], r.Min[Y]))
}

func (r *Rect[S]) Move(dx, dy S) {
	r.Min[X] += dx
	r.Min[Y] += dy
	r.Max[X] += dx
	r.Max[Y] += dy
}

func (r *Rect[S]) BBox() Rect[S] { return *r }

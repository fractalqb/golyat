package golyat

type Axis int

const (
	X Axis = 0
	Y Axis = 1
)

func (a Axis) Cross() Axis { return 1 - a }

func Min[S Scalar](a Axis, s Shape[S]) S { return BBox(s).Min[a] }

func Max[S Scalar](a Axis, s Shape[S]) S { return BBox(s).Max[a] }

func Compare[S Scalar](a Axis, frac S) func(l, r Shape[S]) int {
	return func(l, r Shape[S]) int {
		lf, rf := Frac(a, frac, l), Frac(a, frac, r)
		switch {
		case lf < rf:
			return -1
		case lf > rf:
			return 1
		}
		return 0
	}
}

func Extent[S Scalar](a Axis, s Shape[S]) S { return BBox(s).Extent(a) }

func Extents[S Scalar](a Axis, s ...Shape[S]) (sum S) {
	for _, e := range s {
		sum += Extent(a, e)
	}
	return sum
}

func Frac[S Scalar](a Axis, f S, s Shape[S]) S {
	if r, ok := s.(*Rect[S]); ok {
		return r.Frac(a, f)
	}
	bb := BBox(s)
	return bb.Frac(a, f)
}

func Gap[S Scalar](a Axis, s, t Shape[S]) S {
	sm, tm := Max(a, s), Min(a, t)
	return tm - sm
}

func Margin[S Scalar](a Axis, s, t Shape[S]) (m S) {
	if b, ok := s.(*Border[S]); ok {
		m = b.BBox().Max[a]
	}
	if b, ok := t.(*Border[S]); ok {
		m = max(m, b.BBox().Min[a])
	}
	return m
}

// Force computes the distance the two shapes would have to be moved apart alog
// the axis to avoid an overlap of their bounding boxes. The calculation takes
// into account possible margins that may be created by [Border].
func Force[S Scalar](a Axis, s, t Shape[S]) S {
	f := Gap(a, s, t)
	if f >= 0 {
		m := Margin(a, s, t)
		if f >= m {
			return 0
		}
		return m - f
	}
	return Margin(a, s, t) - f
}

func Move[S Scalar](a Axis, s Shape[S], d S) {
	if a == X {
		s.Move(d, 0)
	} else {
		s.Move(0, d)
	}
}

type Aligner[S Scalar] struct {
	Axis Axis
	Frac S
}

func Align[S Scalar](a Axis, f S) Aligner[S] { return Aligner[S]{a, f} }

func (al Aligner[S]) Shapes(target S, shapes ...Shape[S]) {
	for _, s := range shapes {
		f := Frac(al.Axis, al.Frac, s)
		Move(al.Axis, s, target-f)
	}
}

type Spreader[S Scalar] struct {
	Axis    Axis
	MinFrac S
	MaxFrac S
}

func Spread[S Scalar](a Axis, fmin, fmax S) Spreader[S] {
	return Spreader[S]{a, fmin, fmax}
}

func (sp Spreader[S]) ShapesBetween(min, max S, shapes ...Shape[S]) {
	if len(shapes) == 0 {
		return
	}
	dmin := sp.MinFrac * Extent(sp.Axis, shapes[0])
	dmax := Extent(sp.Axis, shapes[len(shapes)-1])
	dmax -= sp.MaxFrac * dmax
	sp.Shapes(min+dmin, max-dmax, shapes...)
}

func (sp Spreader[S]) Shapes(min, max S, shapes ...Shape[S]) {
	df := sp.MaxFrac - sp.MinFrac
	switch len(shapes) {
	case 0:
		return
	case 1:
		ext := df * Extent(sp.Axis, shapes[0])
		gap := (max - min - ext) / 2
		f := Frac(sp.Axis, sp.MinFrac, shapes[0])
		Move(sp.Axis, shapes[0], min+gap-f)
		return
	}
	var ext S
	for _, s := range shapes {
		ext += df * Extent(sp.Axis, s)
	}
	gap := (max - min - ext) / S(len(shapes)-1)
	for _, s := range shapes {
		f := Frac(sp.Axis, sp.MinFrac, s)
		Move(sp.Axis, s, min-f)
		min = Frac(sp.Axis, sp.MaxFrac, s) + gap
	}
}

package golyat

type Scalar interface{ ~float32 | ~float64 }

type Point[S Scalar] [2]S

func Pt[S Scalar](x, y S) Point[S] { return Point[S]{x, y} }

func (p Point[S]) X() float64 { return float64(p[X]) }
func (p Point[S]) Y() float64 { return float64(p[Y]) }

func (p *Point[S]) Hull(yield func(Point[S]) bool) { yield(*p) }

func (p *Point[S]) Move(dx, dy S) { p[X] += dx; p[Y] += dy }

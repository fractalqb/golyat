# golyat
[![Test Coverage](https://img.shields.io/badge/coverage-14%25-red.svg)](file:coverage.html)
[![Go Report Card](https://goreportcard.com/badge/codeberg.org/fractalqb/golyat)](https://goreportcard.com/report/codeberg.org/fractalqb/golyat)
[![GoDoc](https://godoc.org/codeberg.org/fractalqb/golyat?status.svg)](https://godoc.org/codeberg.org/fractalqb/golyat)

`import "git.fractalqb.de/fractalqb/golyat"`

---

Go Lyaout, or simply golyat, helps to layout shapes that have bounding boxes in
rows or columns just as you find it in most vector drawing programs. However
golyat does only the computations and no drawing at all. Use it e.g. with

- https://github.com/llgcode/draw2d
- https://github.com/fogleman/gg
- https://github.com/tdewolff/canvas

to make something useful from it.

package golyat

import "math"

type Circle[S Scalar] struct {
	C Point[S]
	R S
}

var (
	_ Shape[float64]  = (*Circle[float64])(nil)
	_ BBoxer[float64] = (*Circle[float64])(nil)
)

func (c *Circle[S]) Hull(yield func(Point[S]) bool) {
	rsq2 := c.R / S(math.Sqrt2)
	if !yield(Pt(c.C[X]+c.R, c.C[Y])) {
		return
	}
	if !yield(Pt(c.C[X]+rsq2, c.C[Y]+rsq2)) {
		return
	}
	if !yield(Pt(c.C[X], c.C[Y]+c.R)) {
		return
	}
	if !yield(Pt(c.C[X]-rsq2, c.C[Y]+rsq2)) {
		return
	}
	if !yield(Pt(c.C[X]-c.R, c.C[Y])) {
		return
	}
	if !yield(Pt(c.C[X]-rsq2, c.C[Y]-rsq2)) {
		return
	}
	if !yield(Pt(c.C[X], c.C[Y]-c.R)) {
		return
	}
	yield(Pt(c.C[X]+rsq2, c.C[Y]-rsq2))
}

func (c *Circle[S]) Move(dx, dy S) {
	c.C[X] += dx
	c.C[Y] += dy
}

func (r *Circle[S]) BBox() Rect[S] {
	return Rect[S]{
		Min: Pt(r.C[X]-r.R, r.C[Y]-r.R),
		Max: Pt(r.C[X]+r.R, r.C[Y]+r.R),
	}
}

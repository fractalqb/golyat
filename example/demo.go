package main

import (
	"fmt"
	"math/rand"
	"slices"

	"git.fractalqb.de/fractalqb/golyat"
)

type S = float64

func main() {
	var boxs []golyat.Shape[S]
	const Count = 5
	for i := 0; i < Count; i++ {
		w, h := 5+15*rand.Float64(), 3+10*rand.Float64()
		r := golyat.NewRect(golyat.Pt(-w, w), golyat.Pt(-h, h))
		boxs = append(boxs, r)
	}

	// cnv := canvas.New(200, 20)
	// gc := canvas.NewContext(cnv)
	// gc.Translate(0, 10)
	// gc.SetFillColor(color.NRGBA{255, 50, 0, 128})
	// gc.SetStrokeColor(color.Black)
	// gcBox := func(b *golyat.Rect[S]) {
	// 	gc.MoveTo(b.Min.X(), b.Min.Y())
	// 	gc.LineTo(b.Max.X(), b.Min.Y())
	// 	gc.LineTo(b.Max.X(), b.Max.Y())
	// 	gc.LineTo(b.Min.X(), b.Max.Y())
	// 	gc.Close()
	// }
	for _, s := range boxs {
		fmt.Println("1:", s)
	}

	slices.SortFunc(boxs, golyat.Compare(golyat.X, 0.5))
	golyat.Align(golyat.Y, 0.5).Shapes(0, boxs...)
	golyat.Spread(golyat.X, 0.0, 1.0).Shapes(10, 190, boxs...)

	for _, s := range boxs {
		fmt.Println("2:", s)
	}

	// bb := golyat.BBox(boxs...)
	// gcBox(&bb)
	// gc.Fill()

	// for _, sh := range boxs {
	// 	box := sh.(*golyat.Rect[S])
	// 	gcBox(box)
	// 	gc.Stroke()
	// }

	// renderers.Write("demo.png", cnv, canvas.DPMM(3.2))
}

package golyat

type Border[S Scalar] struct {
	Content         Shape[S]
	Padding, Margin Rect[S]
}

var (
	_ Shape[float64]  = (*Border[float64])(nil)
	_ BBoxer[float64] = (*Border[float64])(nil)
)

func (b *Border[S]) Hull(yield func(Point[S]) bool) {
	b.Content.Hull(yield) // TODO Is this the correct way?
}

func (b *Border[S]) Move(dx, dy S) { b.Content.Move(dx, dy) }

func (b *Border[S]) BBox() Rect[S] {
	bb := BBox(b.Content)
	bb.Min[X] -= b.Padding.Min[X]
	bb.Min[Y] -= b.Padding.Min[Y]
	bb.Max[X] += b.Padding.Max[X]
	bb.Max[Y] += b.Padding.Max[Y]
	return bb
}
